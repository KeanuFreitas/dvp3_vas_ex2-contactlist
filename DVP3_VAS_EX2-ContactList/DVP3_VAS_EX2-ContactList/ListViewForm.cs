﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace DVP3_VAS_EX2_ContactList
{
    public partial class ListViewForm : Form
    {
        public ListViewForm()
        {
            InitializeComponent();
        }

        public void listViewForm_AddPerson(object sender, EventArgs e)
        {
            // Creating a new ListView 
            ListViewItem _listViewForm = new ListViewItem();

            //Grab our data
            Info info = (sender as Form1).userInfo;

            // set our display values: what text to show AND which image
            _listViewForm.Text = info.ToString();

            // adding image to the imageIndex
            _listViewForm.ImageIndex = info.ImageIndex;

            // store a reference of the object so we can access everything
            _listViewForm.Tag = info;

            // add to the listview
            listView1.Items.Add(_listViewForm);

        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            // Delete at selected index
             foreach(ListViewItem a in listView1.SelectedItems)
            {
                listView1.Items.RemoveAt(a.Index);
            }
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            // Alowing the user to change the icon from large to small
            foreach (ListViewItem a in listView1.SelectedItems)
            {
                listView1.View = View.SmallIcon;

                ImageList smallList = new ImageList();
                smallList.ImageSize = new Size(16, 16);
                smallList.ColorDepth = ColorDepth.Depth8Bit;

                listView1.LargeImageList = smallList;
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            // saving the contact list into a text file
            Stream savingFile;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "txt files (*.txt)|*.txt";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if ((savingFile = saveFileDialog1.OpenFile()) != null)
                {
                    using (TextWriter _file = new StreamWriter(savingFile))
                    {

                        _file.WriteLine("Contact List:\n");
                        foreach (var contacts in listView1.Items)
                        {
                            _file.WriteLine(contacts.ToString());
                        }

                        _file.WriteLine("---------------------------------------");
                    }
                    savingFile.Close();
                }
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            // Allowing the user to edit the selected index item from the listview
            foreach (ListViewItem a in listView1.SelectedItems)
            {
                Form1 editInputWindow = new Form1();

                // make sure somthing is being selected or not
                if (listView1.SelectedItems != null)
                {
                    editInputWindow.PopulateForm(Form1.people[a.Index]);
                }

                // making the apply button visible and the add button invisible
                editInputWindow.Owner = this;
                editInputWindow.applyBtn.Visible = true;
                editInputWindow.Owner = this;
                editInputWindow.addBtn.Visible = false;
                editInputWindow.infoSwap += ItemSwapInputForm_infoSwap;

                editInputWindow.ShowDialog();
            }
        }

        private void ItemSwapInputForm_infoSwap(object sender, Form1.ChangingEventArgs e)
        {
            // swaping the index image to chosen one
            listView1.SelectedItems[0].ImageIndex = e.imageIndex;
            listView1.SelectedItems[0].Text = e.firstNameChange + " : " + e.lastNameChange + " : " + e.phoneNumberChange + " : " + e.emailChange;
        }
    }
}
