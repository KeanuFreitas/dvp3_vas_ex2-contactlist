﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace DVP3_VAS_EX2_ContactList
{
    public partial class Form1 : Form
    {
        // using event handler and a list to hold the information of the contacts
        public event EventHandler AddPerson;
        public static List<Info> people = new List<Info>();

        public Info userInfo
        {
            // setters and getters for my contact list information
            get
            {
                // getting the values
                Info userInfo = new Info();
                userInfo.FirstName = fNTxtBx.Text;
                userInfo.LastName = lNTxtBx.Text;
                userInfo.Icon = genderCB.Text;
                userInfo.EmailAddress = eATxtBx.Text;
                userInfo.PhoneNumber = pNTxtBox.Text;
                userInfo.ImageIndex = genderCB.SelectedIndex;
                return userInfo;
            }
            set
            {
                // setting the values
                fNTxtBx.Text = value.FirstName;
                lNTxtBx.Text = value.LastName;
                genderCB.Text = value.Icon;
                eATxtBx.Text = value.EmailAddress;
                pNTxtBox.Text = value.PhoneNumber;
            }
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // exit out of the application
            Application.Exit();
        }

        private void addBtn_Click(object sender, EventArgs e)
        {
            // Checking to see if the number and email address is correct or not.  If not then give a message saying that it is invalid.
            if (Regex.Match(pNTxtBox.Text, @"\(?\d{3}\)?-? *\d{3}-? *-?\d{4}$").Success && eATxtBx.Text.Contains("@")
                && eATxtBx.Text.Contains(".com") && eATxtBx.Text != "")
            {

                // creating a new listViewForm when the add button is clicked
                ListViewForm _list = new ListViewForm();

                // check to see if a ListViewForm is already opened or not
                if (Application.OpenForms.OfType<ListViewForm>().Count() > 0)
                {
                    // attach the event
                    AddPerson += _list.listViewForm_AddPerson;
                    people.Add(userInfo);
                    if (AddPerson != null)
                    {
                        AddPerson(this, new EventArgs());

                        fNTxtBx.Clear();
                        lNTxtBx.Clear();
                        pNTxtBox.Clear();
                        eATxtBx.Clear();
                        genderCB.SelectedIndex = -1;

                    }
                }
                else
                {
                    // attach the event again if needed
                    AddPerson += _list.listViewForm_AddPerson;
                    people.Add(userInfo);
                    _list.Show();
                    if (AddPerson != null)
                    {
                        AddPerson(this, new EventArgs());

                        fNTxtBx.Clear();
                        lNTxtBx.Clear();
                        pNTxtBox.Clear();
                        eATxtBx.Clear();
                        genderCB.SelectedIndex = -1;

                    }
                }
            }
            else
            {
                // clear the email and phone number text boxes when the okay button for the message box is clicked
                DialogResult result = MessageBox.Show("This is not a valid email address or phone number, try again.");
                if (result == DialogResult.OK)
                {
                    pNTxtBox.Clear();
                    eATxtBx.Clear();
                }
            }
        }

        public void PopulateForm(Info _populateForm)
        {
            // populate class with user input and adding to list using the info class
            fNTxtBx.Text = _populateForm.FirstName;
            lNTxtBx.Text = _populateForm.LastName;
            pNTxtBox.Text = _populateForm.PhoneNumber;
            eATxtBx.Text = _populateForm.EmailAddress;
            genderCB.SelectedIndex = _populateForm.ImageIndex;
            genderCB.Text = _populateForm.Icon;

        }

        // Event handler with the new changedeventargs class
        public event EventHandler<ChangingEventArgs> infoSwap;

        public class ChangingEventArgs : EventArgs
        {
            // Create an instance variable for the image index
            public int imageIndex;
            public string firstNameChange;
            public string lastNameChange;
            public string phoneNumberChange;
            public string emailChange;
            public string genderChange;

            // Create a constructor to give this class the image index
            public ChangingEventArgs(int index, string fN, string lN, string pN, string eA, string gender)
            {
                this.imageIndex = index;
                this.firstNameChange = fN;
                this.lastNameChange = lN;
                this.phoneNumberChange = pN;
                this.emailChange = eA;
                this.genderChange = gender;

            }
        }
        private void applyBtn_Click(object sender, EventArgs e)
        {
            // when the apply button is clicked, then change the info from the selected index contact chosen
            if (infoSwap != null)
            {
                string firstName = fNTxtBx.Text;
                string lastName = lNTxtBx.Text;
                string phoneNumber = pNTxtBox.Text;
                string email = eATxtBx.Text;
                string gender = genderCB.Text;
                int imageIndex = genderCB.SelectedIndex;
                infoSwap.ToString();

                infoSwap(this, new ChangingEventArgs(imageIndex, firstName, lastName, phoneNumber, email, gender));
            }
            Close();
        }
    }
}
