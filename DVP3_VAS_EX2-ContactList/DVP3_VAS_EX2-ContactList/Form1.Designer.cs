﻿namespace DVP3_VAS_EX2_ContactList
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CIgrpBx = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.genderCB = new System.Windows.Forms.ComboBox();
            this.addBtn = new System.Windows.Forms.Button();
            this.eATxtBx = new System.Windows.Forms.TextBox();
            this.pNTxtBox = new System.Windows.Forms.TextBox();
            this.lNTxtBx = new System.Windows.Forms.TextBox();
            this.fNTxtBx = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.applyBtn = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.CIgrpBx.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(484, 40);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 36);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(269, 38);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // CIgrpBx
            // 
            this.CIgrpBx.Controls.Add(this.applyBtn);
            this.CIgrpBx.Controls.Add(this.label5);
            this.CIgrpBx.Controls.Add(this.genderCB);
            this.CIgrpBx.Controls.Add(this.addBtn);
            this.CIgrpBx.Controls.Add(this.eATxtBx);
            this.CIgrpBx.Controls.Add(this.pNTxtBox);
            this.CIgrpBx.Controls.Add(this.lNTxtBx);
            this.CIgrpBx.Controls.Add(this.fNTxtBx);
            this.CIgrpBx.Controls.Add(this.label4);
            this.CIgrpBx.Controls.Add(this.label3);
            this.CIgrpBx.Controls.Add(this.label2);
            this.CIgrpBx.Controls.Add(this.label1);
            this.CIgrpBx.Location = new System.Drawing.Point(12, 66);
            this.CIgrpBx.Name = "CIgrpBx";
            this.CIgrpBx.Size = new System.Drawing.Size(439, 413);
            this.CIgrpBx.TabIndex = 1;
            this.CIgrpBx.TabStop = false;
            this.CIgrpBx.Text = "Contact Input:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 321);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(135, 25);
            this.label5.TabIndex = 9;
            this.label5.Text = "Gender Icon:";
            // 
            // genderCB
            // 
            this.genderCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.genderCB.FormattingEnabled = true;
            this.genderCB.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.genderCB.Location = new System.Drawing.Point(24, 349);
            this.genderCB.Name = "genderCB";
            this.genderCB.Size = new System.Drawing.Size(121, 33);
            this.genderCB.TabIndex = 4;
            // 
            // addBtn
            // 
            this.addBtn.Location = new System.Drawing.Point(293, 339);
            this.addBtn.Name = "addBtn";
            this.addBtn.Size = new System.Drawing.Size(128, 51);
            this.addBtn.TabIndex = 8;
            this.addBtn.Text = "Add";
            this.addBtn.UseVisualStyleBackColor = true;
            this.addBtn.Click += new System.EventHandler(this.addBtn_Click);
            // 
            // eATxtBx
            // 
            this.eATxtBx.Location = new System.Drawing.Point(181, 266);
            this.eATxtBx.Name = "eATxtBx";
            this.eATxtBx.Size = new System.Drawing.Size(200, 31);
            this.eATxtBx.TabIndex = 7;
            // 
            // pNTxtBox
            // 
            this.pNTxtBox.Location = new System.Drawing.Point(181, 193);
            this.pNTxtBox.Name = "pNTxtBox";
            this.pNTxtBox.Size = new System.Drawing.Size(200, 31);
            this.pNTxtBox.TabIndex = 6;
            // 
            // lNTxtBx
            // 
            this.lNTxtBx.Location = new System.Drawing.Point(181, 119);
            this.lNTxtBx.Name = "lNTxtBx";
            this.lNTxtBx.Size = new System.Drawing.Size(200, 31);
            this.lNTxtBx.TabIndex = 5;
            // 
            // fNTxtBx
            // 
            this.fNTxtBx.Location = new System.Drawing.Point(181, 47);
            this.fNTxtBx.Name = "fNTxtBx";
            this.fNTxtBx.Size = new System.Drawing.Size(200, 31);
            this.fNTxtBx.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 269);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(156, 25);
            this.label4.TabIndex = 3;
            this.label4.Text = "Email Address:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 193);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(161, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Phone Number:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(54, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Last Name:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(53, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "First Name:";
            // 
            // applyBtn
            // 
            this.applyBtn.Location = new System.Drawing.Point(169, 339);
            this.applyBtn.Name = "applyBtn";
            this.applyBtn.Size = new System.Drawing.Size(103, 51);
            this.applyBtn.TabIndex = 10;
            this.applyBtn.Text = "Apply";
            this.applyBtn.UseVisualStyleBackColor = true;
            this.applyBtn.Visible = false;
            this.applyBtn.Click += new System.EventHandler(this.applyBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 517);
            this.Controls.Add(this.CIgrpBx);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.CIgrpBx.ResumeLayout(false);
            this.CIgrpBx.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.GroupBox CIgrpBx;
        private System.Windows.Forms.TextBox eATxtBx;
        private System.Windows.Forms.TextBox pNTxtBox;
        private System.Windows.Forms.TextBox lNTxtBx;
        private System.Windows.Forms.TextBox fNTxtBx;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox genderCB;
        public System.Windows.Forms.Button applyBtn;
        public System.Windows.Forms.Button addBtn;
    }
}

